import java.util.Scanner;

/**
 * Created by Dmitry Vereykin on 10/8/2015.
 */
public class TestQueueNames {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String inputName = "";

        QueuesNames queuesNames = new QueuesNames();

        while(!inputName.equals("x done")) {
            System.out.print("Input a gender and name (\"x done\" to quit) >");
            //keyboard.nextLine();
            inputName = keyboard.nextLine();

            if (queuesNames.validInput(inputName)) {
                queuesNames.saveInputName(inputName);
            } else {
                continue;
            }

        }

        if (!queuesNames.females.isEmpty())
            System.out.println("Females: " + queuesNames.females);
        if (!queuesNames.males.isEmpty())
            System.out.println("Males: " + queuesNames.males);

    }
}
