import sun.misc.Queue;

/**
 * Created by Dmitry Vereykin on 10/8/2015.
 */
public class QueuesNames {
    ArrayUnbndQueue<String> males;
    ArrayUnbndQueue<String> females;

    public QueuesNames() {
        males = new ArrayUnbndQueue<>();
        females = new ArrayUnbndQueue<>();
    }

    public boolean validInput(String name){
        if (name.substring(0, 2).equals("m ") || name.substring(0, 2).equals("f ")
                                            || name.substring(0, 2).equals("x ")) {
            return true;
        } else {
            System.out.print("Wrong Input!\n");
            return false;
        }
    }
    
    public void saveInputName(String name){
        if (name.substring(0, 2).equals("m ")) {
            males.enqueue(name.substring(2));
        }

        if (name.substring(0, 2).equals("f ")) {
            females.enqueue(name.substring(2));
        }
    }

}
